import random

def display_board(board):
    print('\n' * 100)
    print("   |   | ")
    print(" " + board[7] + " | " + board[8] + " | " + board[9])
    print("   |   | ")
    print("------------")
    print("   |   | ")
    print(" " + board[4] + " | " + board[5] + " | " + board[6])
    print("   |   | ")
    print("------------")
    print("   |   | ")
    print(" " + board[1] + " | " + board[2] + " | " + board[3])
    print("   |   | ")


def player_input():
    '''
    Output = (Player 1 marker, Player 2 marker)
    '''

    marker = ""
    while not (marker == "X" or marker == "O"):
        marker = input("Player1: Choose X or O: ").upper()

    if marker == "X":
        return ("X", "O")
    else:
        return ("O", "X")


def place_marker(board, marker, position):
    board[position] = marker


def win_check(board, mark):
            # Checking rows
    return ((board[1] == mark and board[2] == mark and board[3] == mark) or
            (board[4] == mark and board[5] == mark and board[6] == mark) or
            (board[7] == mark and board[8] == mark and board[9] == mark) or
            # checking columns
            (board[1] == mark and board[4] == mark and board[7] == mark) or
            (board[2] == mark and board[5] == mark and board[8] == mark) or
            (board[3] == mark and board[6] == mark and board[9] == mark) or
            # checking diagonals
            (board[1] == mark and board[5] == mark and board[9] == mark) or
            (board[7] == mark and board[5] == mark and board[3] == mark))


# function that randomly decide which player goes first

def choose_first():
    flip = random.randint(0, 1)
    if flip == 0:
        return "Player 1"
    else:
        return "Player 2"


# function that indicates whether there's available space on the board

def space_check(board, position):
    return board[position] == " "


# function that checks if the board is full return True, otherwise False

def full_board_check(board):
    for i in range(1, 10):
        if space_check(board, i):
            return False
    # board is full if it returns True
    return True


# function that asks for a player's next position and checks if there's a free position

def player_choise(board):
    position = 0
    while position not in [1, 2, 3, 4, 5, 6, 7, 8, 9] or not space_check(board, position):
        position = int(input("Choose a position: (1-9)"))
    return position


# function that asks the players if they want to play again

def replay():
    choice = input("Play again? Enter yes or not")
    return choice == "yes"


# Running the game

print("Welcome to Tic Tac Toe!")

# While loop to keep running the game

while True:
    # play the game

    ##set everything up(board,whos first,choose markers x,o)
    the_board = [" "] * 10
    player1_marker, player2_marker = player_input()

    turn = choose_first()
    print(turn + " will go first")

    play_game = input("Ready to Play? yes or no ? ")

    if play_game == "yes":
        game_on = True
    else:
        game_on = False
    # game play
    while game_on:
        ###player one
        if turn == "Player 1":
            # show the board
            display_board(the_board)
            # choose a position
            position = player_choise(the_board)
            # Place the marker on the position
            place_marker(the_board, player1_marker, position)
            # check if they won
            if win_check(the_board, player1_marker):
                display_board(the_board)
                print("Player 1 has WON!!")
                game_on = False
            # check if there is a tie
            else:
                if full_board_check(the_board):
                    display_board(the_board)
                    print("Tie Game!")
                    game_on = False
                else:
                    turn = "Player 2"
            # no tie no win. The next player's turn

        ###player two
        else:
            display_board(the_board)

            # choose a position
            position = player_choise(the_board)

            # Place the marker on the position
            place_marker(the_board, player2_marker, position)
            # check if they won

            if win_check(the_board, player2_marker):
                display_board(the_board)
                print("Player 2 has WON!!")
                game_on = False
            # check if there is a tie

            else:
                if full_board_check(the_board):
                    display_board(the_board)
                    print("Tie Game!")
                    game_on = False
                else:
                    turn = "Player 1"
            # Break out of the While loop on replay()
    if not replay():
        break
